all: project3 

project3: dikjstra.o graph.o crs.o main.o  
	g++ main.o dikjstra.o graph.o crs.o -o project3

main.o: graph.h main.cpp
	g++ -c main.cpp graph.h -std=c++0x -lpthread

graph.o: graph.cpp graph.h
	g++ -c graph.cpp graph.h -std=c++0x -lpthread

crs.o: crs.h crs.cpp
	g++ -c crs.h crs.cpp -std=c++0x -lpthread

dikjstra.o: dikjstra.h dikjstra.cpp
	g++ -c dikjstra.h dikjstra.cpp -std=c++0x -lpthread

clean:
	rm -rf *.o
	rm -rf *.gch
	rm -rf *.cpp~
	rm -rf *.h~
