#ifndef GRAPH_H
#define GRAPH_H
#include <vector>

using namespace std;

class Vertex;

class Edge {
	public:
		Edge(int org, int dest, int dist);
		int getOrigin();
		int getDestination();
        int getDistance();

	private:
		int origin;
    	int destination;
        int distance;
};

class Vertex {
    public:
		Vertex(int id);
		void addEdge(int v, int distance);
		void changeLabel(int lab);
		int getName();
		int getLabel();
		vector<Edge> getEdges();

	private:
    	int name;		// change to int
    	int label;
    	vector<Edge> edges;
};

/*
    Graph Data structure. Vector of vertices are the vertices of the graph
    and the edges are contained within each vertex. They are directed edges
    from the origin to the destination. Also, we keep the number of edges
    because we need to know how many there are for the CRS function
*/
struct Graph {
    int edges;
    int nodes;
    vector<Vertex> graph;
};


    // Creates a linked list like graph of the size of nodes
    // Number of edge will always equal the 2 * (nodes

    // Can probably move this function to the Graph class

struct Graph createLinkedGraph(int nodes);

/* 
    Creates a dense graph, or one that is half connected

    Number of edges varies

    Can probably move this function to the Graph class

*/
struct Graph createDenseGraph(int nodes);

#endif
