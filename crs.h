#ifndef CRS_H
#define CRS_H
#include <mutex>
#include "graph.h"

using namespace std;

struct node {
	mutex mut;
	int distance;
	int label;
};

struct CRS {
    /* data */
    node *orig;
    int *index;
    int *dest;
    int *edge_data;
    int nodes;
	int edges;
};

struct CRS createCRS(int edges, int n);

struct CRS convertToCRS(struct Graph g);

void printCRS(struct CRS crs);

#endif
