#include <queue>
#include <stack>
#include <stdio.h>
#include <limits.h>
#include "crs.h"

using namespace std;

struct CRS temp;
queue<int> fifo;

void setToInfinity(void) {

	for(int i = 0; i < temp.nodes; i++)
		temp.orig[i].distance = INT_MAX;
}

void dikjstra_fifo(struct CRS crs) {

	temp = crs;
	setToInfinity();

	temp.orig[0].distance = 0;

	fifo.push(0);


	// TODO: Put this part into a method for threads
	while(!fifo.empty()) {
		int orig = fifo.front();
		fifo.pop();
		int end = temp.index[orig];
		int begin;
		if(orig == 0)
			begin = 0;
		else
			begin = temp.index[orig - 1];
		for(int i = begin; i < end; i++) {
			//printf("dest = %i orig = %i\n", temp.dest[i], orig);
			//printf("edge data = %i\n", temp.edge_data[i]);
			int alt = temp.orig[orig].distance + temp.edge_data[i];
			if(alt < temp.orig[temp.dest[i]].distance) {
				//printf("new data = %i\n", alt);
				temp.orig[temp.dest[i]].distance = alt;
				fifo.push(temp.dest[i]);
			}
		}
	}

	for(int i = 0; i < temp.nodes; i++) {
        int dist = temp.orig[i].distance;
        printf("Node %i has distance = %i\n", i, dist);
    }
}
