#ifndef DIKJSTRA_H
#define DIKJSTRA_H

void setToInfinity(void);

void dikjstra_fifo(struct CRS crs);

#endif
