#include <stdio.h>
#include <stdlib.h>
#include <queue>
#include <limits.h>
#include "graph.h"
#include "crs.h"
#include "dikjstra.h"

using namespace std;
/*
    Main driver
*/
int main(){

    struct Graph g = createDenseGraph(5);
    struct CRS crs;
    crs = convertToCRS(g);
	//printCRS(crs);
	dikjstra_fifo(crs);
    
    return 0;
}
